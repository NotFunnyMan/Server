#!/bin/sh
# chkconfig: 2345 20 80
# description: server deamon
# Source function library.
. /etc/init.d/functions

case "$1" in
start)
        echo "$(date) server was started" >> /var/log/server.log
        /usr/local/bin/python3.6 /root/Server/Parser/Parser.py &
        echo $! > /var/run/server.pid
;;
stop)
        echo "$(date) server was stoped" >> /var/log/server.log
        kill $(cat /var/run/server.pid)
        rm -f /var/run/server.pid
;;
restart)
        $0 stop
        $0 start
;;
status)
        if [ -e /var/run/server.pid ];
        then
                echo "Server is running... (pid = $(cat /var/run/server.pid))"
        else
                echo "Server is not running..."
                exit 1
        fi
;;
*)
        echo "Usage: $0 {start|stop|restart|status}"
esac

exit 0
