import os 

log_dir = "/var/log/server/"
log_file = "server.log"
release_file = "release.log"

def Initialization():
    #Create log dir
    if (not os.path.isdir(log_dir)):
        os.mkdir(log_dir)
    pass


def AddToLog(updates):
    for update in updates:
        log = open(release_file, "wa", encoding='utf-8')
        tmp = str(update.__doc__)
        log.write(tmp.split('\n')[0].encode('utf-8').decode('utf-8') + "\n")
        log.write(update.title.encode('utf-8').decode('utf-8') + "\n")
        log.write(update.pubDate.encode('utf-8').decode('utf-8') + "\n")
        log.write("============================================================" + "\n")
    log.close()
    pass