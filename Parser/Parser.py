import time
import logging as log

import NotifySender as notify

import Resources.Lostfilm as lf
import Resources.NewStudio as ns

sleep_time = 300
Lostfilm_initial_value = lf.Lostfilm()
NewStudio_initial_value = ns.NewStudio()

def Initialization():
    global Lostfilm_initial_value, NewStudio_initial_value
    start_time = time.time()
    Lostfilm_initial_value = lf.Initialization()
    print("---LF: %s seconds ---" % (time.time() - start_time))
    start_time = time.time()
    NewStudio_initial_value = ns.Initialization()
    print("---NS: %s seconds ---" % (time.time() - start_time))
    #logger.Initialization()


if __name__ == "__main__":
    log.basicConfig(level = log.ERROR, filename = '../../server.log', format = u"%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s")

    Initialization()
    while True:
        LFupdates = lf.CheckUpdates(Lostfilm_initial_value)
        if (LFupdates):
            notify.Send(LFupdates, lf.RESOURCE)
            Lostfilm_initial_value = LFupdates[-1]
            
        NSupdates = ns.CheckUpdates(NewStudio_initial_value)
        if (NSupdates):
            notify.Send(NSupdates, ns.RESOURCE)
            NewStudio_initial_value = NSupdates[-1]
        time.sleep(sleep_time)
    pass